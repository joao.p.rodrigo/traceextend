
$(document).ready(() => {
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.type === 'attributes' && $('.table-results .waitMe_container').length === 0) {
                setTimeout(() => {
                    getTaskSearchParams()
                    observer.disconnect()
                }, 200);

            }
        })
    })
    observer.observe(document.getElementsByClassName("table-results")[0], {
        childList: false
        , subtree: false
        , attributes: true
        , characterData: false
    })

    // stop watching using:
    //observer.disconnect()
    // setTimeout(() => {
    //     $('#tableTasks').DataTable( {
    //         "initComplete": function(settings, json) {
    //           alert( 'DataTables has finished its initialisation.' );
    //         }
    //       } );
    // }, 5000);

    //getTaskSearchParams()
})


$('#filterSearch').on('click', function () {
    setTaskSearchParams()
})

function setTaskSearchParams() {
    if ('URLSearchParams' in window) {
        var searchParams = new URLSearchParams(window.location.search)
        var newParams = {
            showAll: $('#showEndedTasks')[0].checked,
            healthUnitParent: $('#Select2HealthUnitLevel6').find(':selected')[0].value,
            pSelect2PublicHealthUnitId: $('#Select2PublicHealthUnit').find(':selected')[0].value,
            pSelect2HealthUnitId: $('#Select2HealthUnit').find(':selected')[0].value,
            pSelect2Clinical: $('#Select2Clinical').find(':selected')[0].value,
            pSelect2SurveillanceStatus: $('#Select2SurveillanceStatus').find(':selected')[0].value
        }
        Object.keys(newParams).forEach(
            function (k) {
                searchParams.set(k, newParams[k]);
            }
        )
        var newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
        history.pushState(null, '', newRelativePathQuery);
    }
}

function getTaskSearchParams() {
    if ('URLSearchParams' in window) {
        var changed = false;
        var searchParams = new URLSearchParams(window.location.search);
        var healthUnitParent = searchParams.get("healthUnitParent");
        var pSelect2HealthUnitId = searchParams.get("pSelect2HealthUnitId")
        var genericParams = {
            '#Select2PublicHealthUnit': searchParams.get("pSelect2PublicHealthUnitId"),
            '#Select2Clinical': searchParams.get("pSelect2Clinical"),
            '#Select2SurveillanceStatus': searchParams.get("pSelect2SurveillanceStatus")
        }
        $('#showEndedTasks')[0].checked = searchParams.get("showAll") === "true"
        if (healthUnitParent !== null && pSelect2HealthUnitId !== null) {
            $('#Select2HealthUnitLevel6')
                .val(healthUnitParent)[0].dispatchEvent(new Event("change"))

            runLocal(`loadHealthUnitId("#Select2HealthUnit", ${healthUnitParent}, true)`)
            $('#Select2HealthUnit')
                .val(pSelect2HealthUnitId)
            [0].dispatchEvent(new Event("change"))
            changed = true;
        }
        Object.keys(genericParams).forEach(function (k) {
            if (genericParams[k] !== null) {
                $(k).val(genericParams[k])[0].dispatchEvent(new Event("change"))
                changed = true;
            }
        })
        if (changed) runLocal(`TriggerRefreshDataTable()`)
    }
}

function runLocal(scriptText) {
    var script = document.createElement('script');
    script.textContent = scriptText;
    (document.head || document.documentElement).appendChild(script);
    script.remove();
}
